@extends('layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title text-center">
                        <h2>最新消息
                        <span class="badge badge-primary">News</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">消息排序</th>
                                                <th class="text-center">消息標題</th>
                                                <th class="text-center">消息關鍵字</th>
                                            </thead>
                                            <tbody id="news">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 消息詳細資訊 -->
<div class="modal fade" id="read_news" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">消息詳細資訊</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="read_news" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label col-md-2  text-left" for="read_title">消息標題：</label>
                        {{-- <div id="addtitle" class="col-md-12 col-sm-12 col-xs-12 text-lift"> --}}
                        <div  class="col-md-12 col-sm-12 col-xs-12 text-lift">
                            <input type="text" name="readtitle" id="readtitle" class="form-control col-md-12 col-xs-12" placeholder="" disabled  style="border-style:none;background-color:white;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="read_price">消息關鍵字：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="readkeyword" id="readkeyword" class="form-control col-md-12 col-xs-12" placeholder="" disabled style="border-style:none;background-color:white;">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-2 text-left" for="read_content">消息內容：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row='4' col='50' class="form-control" name="readcontent" id="readcontent"  placeholder="" disabled style="border-style:none;background-color:white;"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Member/News.js') }}"></script>
@endsection