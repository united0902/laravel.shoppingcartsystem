@extends('layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title text-center">
                        <h2>我的訂單
                        <span class="badge badge-success">Order</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">商品排序</th>
                                                <th class="text-center">訂單編號</th>
                                                <th class="text-center">商品名稱</th>
                                                <th class="text-center">商品價格</th>
                                                <th class="text-center">商品數量</th>
                                                <th class="text-center">總價格</th>
                                                <th class="text-center">訂購帳號</th>
                                                <th class="text-center">訂單日期及時間</th>
                                            </thead>
                                            <tbody id="order">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Member/Order.js') }}"></script>
@endsection