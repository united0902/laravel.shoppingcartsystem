<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/Welcome.css') }}" rel="stylesheet">
        <title>PUBG絕地求生寶物交易網</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">首頁</a>
                    @else
                        <a href="{{ route('login') }}">登入</a>
                        <a href="{{ route('register') }}">註冊</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    WelcomeToShopping
                </div>

                <div class="links">
                    <a href="news">最新消息</a>
                    <a href="users">購物會員</a>
                    <a href="product">精彩商品</a>
                    <a href="cart">購物車</a>
                    <a href="order">我的訂單</a>
                </div>
            </div>
        </div>
    </body>
</html>
