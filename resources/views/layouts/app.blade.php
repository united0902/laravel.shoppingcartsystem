<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('toastr-master/build/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('toastr-master/build/toastr.css')}}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>PUBG絕地求生寶物交易網</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!--左上角的連結 -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{-- {{ config('app.name', 'Laravel') }} --}}
                        ShoppingSystem
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- 左上角的連結 -->
                    <ul class="nav navbar-nav">
                        <li><a href="home">首頁</a></li>
                        <li><a href="news">最新消息</a></li>
                        <li><a href="product">精彩商品</a></li>
                        <li><a href="order">我的訂單</a></li>
                    </ul>

                    <!-- 右上角的連結 -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest  <!--訪客端看到的連結 -->
                            <li><a href="cart">購物車</a></li>
                            <li><a href="http://127.0.0.1:8000/shoppingsystem/login">管理端</a></li>
                            <li><a href="{{ route('login') }}">登入</a></li>
                            <li><a href="{{ route('register') }}">註冊</a></li>
                        @else   <!--會員端看到的連結 -->
                            <li><a href="cart">購物車</a></li>
                            <li><a href="users">個人資訊</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            登出
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- JavaScripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('toastr-master/build/toastr.min.js') }}"></script>
    <script src="{{ asset('toastr-master/build/toastr.js.map') }}"></script>
    <script src="{{ asset('toastr-master/toastr.js') }}"></script>
    <script src="{{asset('background/ckeditor/ckeditor.js')}}"></script>
    <script>var base_url = "{{url('')}}";</script>
    @yield('script')
</body>
</html>
