@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">會員資料</div>

                <div class="panel-body">
                    <form class="form-horizontal" id="update">

                        <div class="form-group">
                        <label for="account" class="col-md-4 control-label">會員帳號：</label>
                            <div class="col-md-6">
                                <input id="account" type="text" class="form-control" name="account" required autofocus readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                        <label for="password" class="col-md-4 control-label">會員新密碼：</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                            </div>

                        <div class="form-group">
                            <label for="password_confirmation" class="col-md-4 control-label">會員新密碼確認：</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">會員姓名：</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"  required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sex" class="col-md-4 control-label">會員性別：</label>
                            <div class="col-md-6">
                                <select id="sex" class="form-control" name="sex" required autofocus>
                                        <option>請選擇您的性別
                                        <option>男
                                        <option>女
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">會員電子信箱：</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">會員手機電話：</label>
    
                            <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control" name="phone"  required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="adds" class="col-md-4 control-label">會員居住地址：</label>
    
                            <div class="col-md-6">
                                <input id="adds" type="adds" class="form-control" name="adds" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">確認修改</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Member/Users.js') }}"></script>
@endsection