@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">會員註冊</div>

                <div class="panel-body">
                    <form class="form-horizontal" id="create">

                        <div class="form-group">
                        <label for="account" class="col-md-4 control-label">註冊帳號：</label>
                            <div class="col-md-6">
                                <input id="account" type="text" class="form-control" name="account" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="password" class="col-md-4 control-label">註冊密碼：</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                         </div>

                        <div class="form-group">
                            <label for="password_confirmation" class="col-md-4 control-label">註冊密碼確認：</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">您的姓名：</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"  required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sex" class="col-md-4 control-label">您的性別：</label>
                            <div class="col-md-6">
                                <select id="sex" class="form-control" name="sex" required autofocus>
                                        <option>請選擇您的性別
                                        <option>男
                                        <option>女
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">註冊電子信箱：</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">您的手機電話：</label>
    
                            <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control" name="phone"  required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="adds" class="col-md-4 control-label">您的居住地址：</label>
    
                            <div class="col-md-6">
                                <input id="adds" type="adds" class="form-control" name="adds" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">確認註冊</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>

    $( '#create' ).submit(function( event )
    {
        event.preventDefault();//不轉頁

        
        // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
        var data = {
            'account' : $('#account').val(),
            'password': $('#password').val(),
            'password_confirmation': $('#password_confirmation').val(),
            'name'    : $('#name').val(),
            'sex'     : $('#sex').val(),
            'adds'    : $('#adds').val(),
            'email'   : $('#email').val(),
            'phone'   : $('#phone').val(),
        }
        // console.log(data);
        
            $.ajax({
            url:'/register', //127.0.0.1:8000/shoppingsystem/member/add
            type:'post', //傳輸方式
            data:data, //上面data的所有值
            dataType:'json', //json格式
            headers: //避免跨站偽造請求攻擊
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(result){
                // $('#response').text(result);
                if(result.message === "Success"){
                    // alert("註冊成功");
                    // window.location.href="login" ;
                    toastr.success("註冊成功");
                    setTimeout("window.location.href='login'",1500);
                }
                // result.value
                // console.log(result);
            },
            error: function(xhr,status,error) 
            {
                //console.log(xhr.responseJSON.errors);
                
                if(xhr.status == 422)//如果他是422的錯誤,就執行以下動作
                {
                    var error_msg = ''; //宣告error_msg為空
                    $.each( xhr.responseJSON.errors, function( key, value ) { //跑迴圈顯示錯誤
                        error_msg += value[0] +"<br>"; //顯示欄位名稱＋錯誤原因並做換行動作
                    });
                    // alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
                    toastr.error(error_msg);
                    }
            }

        });
    });
    
</script>

@endsection
