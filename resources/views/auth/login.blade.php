@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center">會員登入</div>
                
                <div class="panel-body">
                    <form class="form-horizontal" id="login">

                        <div class="form-group">
                            <label for="account" class="col-md-4 control-label">帳號</label>
                            <div class="col-md-6">
                                <input id="account" type="account" class="form-control" name="account" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">密碼</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 記住我
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    登入
                                </button>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    忘記密碼?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>

    $( '#login' ).submit(function( event )
    {
        event.preventDefault();//不轉頁
        
        // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
        var data = {
            'account' : $('#account').val(),
            'password': $('#password').val(),
        }
        // console.log(data);
        
            $.ajax({
            url:'/login', //127.0.0.1:8000/login
            type:'post', //傳輸方式
            data:data, //上面data的所有值
            dataType:'json', //json格式
            headers: //避免跨站偽造請求攻擊
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(result){
                // console.log(result)
                // $('#response').text(result);
                if(result.message === "Success"){
                    // alert("登入成功");
                    // window.location.href="home"; 
                    toastr.success("登入成功");
                    setTimeout("window.location.href='home'",1500);
                }
                if(result.message === "False"){
                    // alert("無此帳號,請重新登入");
                    // window.location.href="login";
                    toastr.error("登入失敗,重新登錄或請註冊");
                    setTimeout("window.location.href='login'",1500); 
                }
                // // result.value
                // // console.log(result);
            },
            error: function(xhr,status,error) 
            {
                //console.log(xhr.responseJSON.errors);
                
                if(xhr.status == 422)//如果他是422的錯誤,就執行以下動作
                {
                    var error_msg = ''; //宣告error_msg為空
                    $.each( xhr.responseJSON.errors, function( key, value ) { //跑迴圈顯示錯誤
                        error_msg += value[0] +'\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                    });
                    alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
                    }
            }

        });
    });
    
</script>
@endsection
