@extends('layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title text-center">
                        <h2>精彩商品
                        <span class="badge badge-success">Product</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">商品排序</th>
                                                <th class="text-center">商品名稱</th>
                                                <th class="text-center">商品圖片</th>
                                                <th class="text-center">商品價格(TWD)</th>
                                                <th class="text-center">商品庫存</th>
                                                <th class="text-center">操作</th>
                                            </thead>
                                            <tbody id="product">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 商品詳細資訊 -->
<div class="modal fade" id="product_content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">商品詳細資訊</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="add_cart" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label col-md-2  text-left" for="add_title">商品名稱：</label>
                        <div  class="col-md-12 col-sm-12 col-xs-12 text-lift">
                            <input type="text" name="addtitle" id="addtitle" class="form-control col-md-12 col-xs-12" placeholder="" disabled  style="border-style:none;background-color:white;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="add_images">商品圖片：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="" id="addpicture" width="100" height="100" disabled>
                            <input type="file" name="addimages" style="display:none" id="addimages" class="form-control col-md-12 col-xs-12" placeholder="" disabled style="border-style:none;background-color:white;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 text-left" for="add_price">商品價格(TWD)：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="addprice" id="addprice" class="form-control col-md-12 col-xs-12" placeholder="" disabled style="border-style:none;background-color:white;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="add_enough">商品庫存：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="addenough" id="addenough" class="form-control col-md-12 col-xs-12" placeholder="" disabled style="border-style:none;background-color:white;">  
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-2 text-left" for="add_content">商品描述：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row='4' col='50' class="form-control" name="addcontent" id="addcontent"  placeholder="" disabled style="border-style:none;background-color:white;"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">加入購物車</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>var url = "{{ asset('images') }}";</script>
<script src="{{ asset('js/Member/Product.js') }}"></script>
@endsection