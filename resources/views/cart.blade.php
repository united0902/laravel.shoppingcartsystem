@extends('layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title text-center">
                        <h2>我的購物車
                        <span class="badge badge-success">ProductCart</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form id="checkout" onSubmit="return checkform()";>
                                        <table class="table table-striped" id="ty">
                                            <thead>
                                                <th class="text-center">商品排序</th>
                                                <th class="text-center">商品名稱</th>
                                                <th class="text-center">商品圖片</th>
                                                <th class="text-center">商品價格</th>
                                                <th class="text-center">商品數量</th>
                                                <th class="text-center">操作</th>
                                            </thead>
                                            <tbody id="cart">
                                            </tbody>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"><button class="btn btn-success" type="submit" id="check_out">確定購買</button></td>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 刪除 -->
<div class="modal fade" id="cart_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">商品刪除</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="del" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_title">確定要刪除嘛?</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" id="id">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-danger">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Member/Cart.js') }}"></script>
<script>
var url = "{{ asset('images') }}";
</script>
@endsection