@extends('manager.layouts.users')
@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header text-center">ShoppingSystem</div>
        <div class="card-header">
            <input type="button" class="btn btn-primary btn-block"  onclick="javascript:location.href='http://127.0.0.1:8000/login'" value="回會員端登入"></button>
        </div>
            <div class="card-body">

            <form id="manager_login">
                <div class="form-group">
                <div class="form-label-group">
                    <input type="text" id="account" class="form-control" placeholder="account" required="required" autofocus="autofocus">
                    <label for="account">您的帳號</label>
                </div>
                </div>
                <div class="form-group">
                <div class="form-label-group">
                    <input type="password" id="password" class="form-control" placeholder="password" required="required">
                    <label for="password">您的密碼</label>
                </div>
                </div>
                <div class="form-group">
                <div class="checkbox">
                    <label>
                    <input type="checkbox" value="remember-me">
                    記住我的密碼
                    </label>
                </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">登入</button>
            </form>
            <div class="text-center">
                <a class="d-block small" href="forgot-password.html">忘記密碼?</a>
            </div>
        </div>
    </div>
</div>
@section('script')
<script src="{{ asset('js/Manager/Users/Login.js') }}"></script>
@endsection