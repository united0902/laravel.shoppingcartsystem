@extends('manager.layouts.layouts')
@section('title')
<li class="breadcrumb-item active">會員管理</li>
@endsection
@section('content')
{{-- 下面為會員列表 --}}
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>會員列表
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#adduser">新增會員</button>
                        </h2>
                        <div class="clearfix"></div>
                        <div>
                            會員帳號：<input type="text" name="users_account" id="users_account">
                            會員姓名：<input type="text" name="users_name" id="users_name">
                            會員信箱：<input type="text" name="users_email" id="users_email">
                            會員地址：<input type="text" name="users_adds" id="users_adds">
                            會員電話：<input type="text" name="users_phone" id="users_phone">
                            <button type="button" id="select">查詢</button>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">會員編號</th>
                                                <th class="text-center">會員帳號</th>
                                                <th class="text-center">會員姓名</th>
                                                <th class="text-center">會員性別</th>
                                                <th class="text-center">電子信箱</th>
                                                <th class="text-center">居住地址</th>
                                                <th class="text-center">手機電話</th>
                                                <th class="text-center">管理操作</th>
                                            </thead>
                                            <tbody id="show">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
{{-- 下面為會員修改 --}}
<div class="modal fade" id="editmember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">修改會員資料</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="edit_member" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_account">會員帳號</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {{-- <input type="hidden" name="edit_member_account" id="edit_member_account" class="form-control col-md-12 col-xs-12" disabled placeholder="請輸入會員帳號"> --}}
                            <input type="text" name="edit_member_account" id="edit_member_account" class="form-control col-md-12 col-xs-12" disabled placeholder="請輸入會員帳號">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_name">會員姓名</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="edit_member_name" id="edit_member_name" class="form-control col-md-12 col-xs-12" placeholder="請輸入會員姓名">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_sex">會員性別</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <select  class="form-control" name="edit_member_sex" id="edit_member_sex"  placeholder="性別">
                                    <option>請選擇您的性別
                                    <option>男
                                    <option>女
                                </select> 
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_email">電子信箱</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_member_email" id="edit_member_email" class="form-control col-md-12 col-xs-12" placeholder="請輸入電子信箱">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_adds">居住地址</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_member_adds" id="edit_member_adds" class="form-control col-md-12 col-xs-12" placeholder="請輸入居住地址">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_member_phone">手機電話</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_member_phone" id="edit_member_phone" class="form-control col-md-12 col-xs-12" placeholder="請輸入手機電話">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">修改</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 下面為會員刪除 -->
<div class="modal fade" id="del_member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">刪除會員</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="del" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_account">確定要刪除嘛？</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_account" id="del_member_account">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_password"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_accoun" id="del_member_password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_name"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_name" id="del_member_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_sex"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_sex" id="del_member_sex">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_email"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_email" id="del_member_email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_adds"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_adds" id="del_member_adds">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_member_phone"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_member_phone" id="del_member_phone">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- 下面為管理員列表 --}}
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>管理員列表
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addmanager">新增管理員</button>
                        </h2>
                        <div class="clearfix"></div>
                        <div>
                            管理員帳號：<input type="text" name="managers_account" id="managers_account">
                            管理員姓名：<input type="text" name="managers_name" id="managers_name">
                            管理員信箱：<input type="text" name="managers_email" id="managers_email">
                            管理員地址：<input type="text" name="managers_adds" id="managers_adds">
                            管理員電話：<input type="text" name="managers_phone" id="managers_phone">
                            <button type="button" id="selects">查詢</button>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">管理員編號</th>
                                                <th class="text-center">管理員帳號</th>
                                                <th class="text-center">管理員姓名</th>
                                                <th class="text-center">管理員電子信箱</th>
                                                <th class="text-center">管理員居住地址</th>
                                                <th class="text-center">管理員手機電話</th>
                                                <th class="text-center">管理操作</th>
                                            </thead>
                                            <tbody id="shows">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 新增會員 -->
<div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">會員新增</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="createusers" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="account">會員帳號</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="account" id="account" class="form-control col-md-12 col-xs-12" placeholder="請輸入帳號">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="password">會員密碼</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="password" id="password" class="form-control col-md-12 col-xs-12" placeholder="請輸入密碼">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="password_confirmation">密碼確認</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control col-md-12 col-xs-12" placeholder="請再次輸入密碼">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="name">會員姓名</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="name" id="name" class="form-control col-md-12 col-xs-12" placeholder="請輸入姓名">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="sex">會員性別</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {{-- <input type="text" name="sex" id="sex" class="form-control col-md-12 col-xs-12" placeholder="請輸入性別"> --}}
                            <select  class="form-control" name="sex" id="sex"  placeholder="性別">
                                <option>請選擇您的性別
                                <option>男
                                <option>女
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="adds">會員地址</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="adds" id="adds" class="form-control col-md-12 col-xs-12" placeholder="請輸入地址">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="email">會員信箱</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="email" id="email" class="form-control col-md-12 col-xs-12" placeholder="請輸入電子信箱">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="phone">會員手機</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="phone" id="phone" class="form-control col-md-12 col-xs-12" placeholder="請輸入手機號碼">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">新增</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 新增管理員 -->
<div class="modal fade" id="addmanager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">管理員新增</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="manager" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="manager_account">管理員帳號</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="manager_account" id="manager_account" class="form-control col-md-12 col-xs-12" placeholder="請輸入帳號">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="manager_password">管理員密碼</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="manager_password" id="manager_password" class="form-control col-md-12 col-xs-12" placeholder="請輸入密碼">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="manager_password_confirmation">管理員密碼確認</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="manager_password_confirmation" id="manager_password_confirmation" class="form-control col-md-12 col-xs-12" placeholder="請再次輸入密碼">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="name">管理員姓名</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="manager_name" id="manager_name" class="form-control col-md-12 col-xs-12" placeholder="請輸入姓名">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="adds">管理員地址</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="manager_adds" id="manager_adds" class="form-control col-md-12 col-xs-12" placeholder="請輸入地址">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="email">管理員信箱</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="manager_email" id="manager_email" class="form-control col-md-12 col-xs-12" placeholder="請輸入電子信箱">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="phone">管理員手機</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="manager_phone" id="manager_phone" class="form-control col-md-12 col-xs-12" placeholder="請輸入手機號碼">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">新增</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- 下面為管理員修改 --}}
<div class="modal fade" id="editmanager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">修改管理員資料</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="edit_manager" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_manager_account">管理員帳號</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {{-- <input type="hidden" name="edit_manager_account" id="edit_manager_account" class="form-control col-md-12 col-xs-12" disabled placeholder="請輸入管理員帳號"> --}}
                            <input type="text" name="edit_manager_account" id="edit_manager_account" class="form-control col-md-12 col-xs-12" disabled placeholder="請輸入管理員帳號">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_manager_name">管理員姓名</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="edit_manager_name" id="edit_manager_name" class="form-control col-md-12 col-xs-12" placeholder="請輸入管理員姓名">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_manager_email">管理員電子信箱</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_manager_email" id="edit_manager_email" class="form-control col-md-12 col-xs-12" placeholder="請輸入管理員電子信箱">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_manager_adds">管理員居住地址</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_manager_adds" id="edit_manager_adds" class="form-control col-md-12 col-xs-12" placeholder="請輸入管理員居住地址">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_manager_phone">管理員手機電話</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="edit_manager_phone" id="edit_manager_phone" class="form-control col-md-12 col-xs-12" placeholder="請輸入管理員手機電話">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">修改</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 下面為管理員刪除 -->
<div class="modal fade" id="del_manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">刪除管理員</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="dels" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_account">確定要刪除嘛？</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_account" id="del_manager_account">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_password"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_accoun" id="del_manager_password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_name"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_name" id="del_manager_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_email"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_email" id="del_manager_email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_adds"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_adds" id="del_manager_adds">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_manager_phone"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="del_manager_phone" id="del_manager_phone">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>  
@endsection
@section('script')
<script src="{{ asset('js/Manager/Member/index.js') }}"></script>
@endsection