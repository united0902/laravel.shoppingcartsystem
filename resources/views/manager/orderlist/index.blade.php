@extends('manager.layouts.layouts')
@section('title')
<li class="breadcrumb-item active">訂單管理</li>
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>訂單列表</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">訂單編號</th>
                                                <th class="text-center">訂購會員</th>
                                                <th class="text-center">訂購商品</th>
                                                <th class="text-center">訂購數量</th>
                                                <th class="text-center">訂購價格</th>
                                                <th class="text-center">訂購總價格</th>
                                                <th class="text-center">管理操作</th>
                                            </thead>
                                            <tbody id="order">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 刪除 -->
<div class="modal fade" id="del_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">商品刪除</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="del" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_title">確定要刪除嘛?</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" id="id">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Manager/Orderlist/index.js') }}"></script>
@endsection