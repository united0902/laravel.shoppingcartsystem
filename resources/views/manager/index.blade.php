@extends('manager.layouts.layouts')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>首頁商品列表
                        <span class="badge badge-success">Product</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">商品排序</th>
                                                <th class="text-center">商品名稱</th>
                                                <th class="text-center">商品圖片</th>
                                                <th class="text-center">商品價格(TWD)</th>
                                                <th class="text-center">商品內容</th>
                                            </thead>
                                            <tbody id="product">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>首頁消息列表
                        <span class="badge badge-primary">News</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">消息排序</th>
                                                <th class="text-center">消息標題</th>
                                                <th class="text-center">消息關鍵字</th>
                                                <th class="text-center">消息內容</th>
                                            </thead>
                                            <tbody id="news">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>首頁會員列表
                            <span class="badge badge-warning">Members</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">會員編號</th>
                                                <th class="text-center">會員帳號</th>
                                                <th class="text-center">會員姓名</th>
                                                <th class="text-center">會員性別</th>
                                                <th class="text-center">電子信箱</th>
                                                <th class="text-center">居住地址</th>
                                                <th class="text-center">手機電話</th>
                                            </thead>
                                            <tbody id="member">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>首頁管理員列表
                            <span class="badge badge-danger">Managers</span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">管理員編號</th>
                                                <th class="text-center">管理員帳號</th>
                                                <th class="text-center">管理員姓名</th>
                                                <th class="text-center">管理員電子信箱</th>
                                                <th class="text-center">管理員居住地址</th>
                                                <th class="text-center">管理員手機電話</th>
                                            </thead>
                                            <tbody id="manager">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/index.js') }}"></script>
<script>
var url= "{{ asset('images') }}";
</script>
@endsection