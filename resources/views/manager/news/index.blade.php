@extends('manager.layouts.layouts')
@section('title')
<li class="breadcrumb-item active">消息管理</li>
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>消息列表
                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addnews">新增消息</button>
                        </h2>
                        <div class="clearfix"></div>
                        <div>
                            消息標題：<input type="text" name="news_title" id="news_title">
                            消息關鍵字：<input type="text" name="news_keyword" id="news_keyword">
                            消息內容：<input type="text" name="news_content" id="news_content">
                            <button type="button" id="select">查詢</button>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">消息排序</th>
                                                <th class="text-center">消息標題</th>
                                                <th class="text-center">消息關鍵字</th>
                                                <th class="text-center">管理操作</th>
                                            </thead>
                                            <tbody id="show">

                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
 </div>
<!-- 新增 -->
<div class="modal fade" id="addnews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">新增消息</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="create" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="title">消息標題</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="title" id="title" class="form-control col-md-12 col-xs-12" placeholder="請輸入消息標題">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="keyword">消息關鍵字</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="keyword" id="keyword" class="form-control col-md-12 col-xs-12" placeholder="請輸入關鍵字">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="content">消息內容</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row="4" col="50" class="form-control" name="content" id="content"  placeholder="請輸入消息描述"></textarea>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">新增</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 修改 -->
<div class="modal fade" id="editnews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">修改消息</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="edit" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_title">消息標題</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="edittitle" id="edittitle" class="form-control col-md-12 col-xs-12" placeholder="請輸入消息標題">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_keyword">消息關鍵字</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="editkeyword" id="editkeyword" class="form-control col-md-12 col-xs-12" placeholder="請輸入關鍵字">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_content">消息內容</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row='4' col='50' class="form-control" name="editcontent" id="editcontent"  placeholder="請輸入消息描述"></textarea>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">修改</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 刪除 -->
<div class="modal fade" id="delnews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">刪除消息</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="del" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_title">確定要刪除嘛?</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="deltitle" id="deltitle">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_keyword"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="delkeyword" id="delkeyword">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_content"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="delcontent" id="delcontent">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/Manager/News/index.js') }}"></script>
@endsection
