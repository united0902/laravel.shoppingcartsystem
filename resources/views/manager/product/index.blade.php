@extends('manager.layouts.layouts')
@section('title')
<li class="breadcrumb-item active">商品管理</li>
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>商品列表
                            <span class="badge badge-success">Products</span>
                        </h2>
                        <div>
                            商品名稱：<input type="text" name="titles" id="titles">
                            商品價格(TWD)：<input type="text" name="prices" id="prices">
                            商品庫存：<input type="text" name="enoughs" id="enoughs">
                            商品描述：<input type="text" name="contents" id="contents">
                            <button type="button" id="select">查詢</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_panel">
                            <div class="x_panel">
                                <div>
                                    <form>
                                        <table class="table table-striped">
                                            <thead>
                                                <th class="text-center">商品編號</th>
                                                <th class="text-center">商品名稱</th>
                                                <th class="text-center">商品圖片</th>
                                                <th class="text-center">商品價格(TWD)</th>
                                                <th class="text-center">商品庫存</th>
                                                <th class="text-center">管理操作</th>
                                            </thead>
                                            <tbody id="product">
                                            </tbody>
                                        </table>
                                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addproduct">新增商品</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- 新增商品 -->
<div class="modal fade" id="addproduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">新增商品</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="create" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="title">商品名稱：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="title" id="title" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品名稱">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="image">商品圖片：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="file" name="image" id="image" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品圖片">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="enough">商品庫存：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="number" min="1" name="enough" id="enough" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品庫存">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="price">商品價錢(TWD)：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="number" min="1" name="price" id="price" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品價錢">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="content">商品內容：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row="4" col="50" class="form-control" name="content" id="content"  placeholder="請輸入商品內容"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">新增</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 修改 -->
<div class="modal fade" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <input type="hidden" id="id">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">修改商品</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="edit" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_title">商品名稱：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="edittitle" id="edittitle" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品名稱">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_images">商品圖片：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="" id="picture" width="100" height="100">
                            <input type="file" name="editimages" id="editimages" class="form-control col-md-12 col-xs-12" placeholder="請選擇商品圖片">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_price">商品價格(TWD)：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="number" name="editprice" id="editprice" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品價格(TWD)">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_enough">商品庫存：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="number" name="editenough" id="editenough" class="form-control col-md-12 col-xs-12" placeholder="請輸入商品庫存">
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="edit_content">商品描述：</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea row='4' col='50' class="form-control" name="editcontent" id="editcontent"  placeholder="請輸入商品描述"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">修改</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 刪除 -->
<div class="modal fade" id="delproduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:650px">
                
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">商品刪除</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>

            <div class="modal-body">
                <br>
                <form id="del" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-left" for="del_title">確定要刪除嘛?</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" id="id">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-success">刪除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>var url = "{{ asset('images') }}";</script>
<script src="{{ asset('js/Manager/Product/index.js') }}"></script>
@endsection