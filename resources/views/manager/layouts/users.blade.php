<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PUBG絕地求生寶物交易網</title>

    <link href="{{asset('background/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('background/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('background/css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('toastr-master/build/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('toastr-master/build/toastr.css')}}" rel="stylesheet">
  </head>

    <body class="bg-dark">
            @yield('content')
    </body>
    
    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('background/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('background/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('background/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('toastr-master/build/toastr.min.js') }}"></script>
    <script src="{{ asset('toastr-master/build/toastr.js.map') }}"></script>
    <script src="{{ asset('toastr-master/toastr.js') }}"></script>
    <script>var base_url = "{{url('')}}";</script>
    <script src="{{asset('background/ckeditor/ckeditor.js')}}"></script>

     @yield('script')

</html>