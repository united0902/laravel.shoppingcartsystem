<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//原始範例：
Route::get('/', function () {
    return view('welcome');
});

Route::get('news', 'VisitorController@news');
Route::post('news_show', 'VisitorController@news_show');
Route::get('users', 'VisitorController@users');
Route::get('users_show', 'VisitorController@users_show');
Route::post('users_update', 'VisitorController@users_update');
Route::get('product', 'VisitorController@product');
Route::post('product_show', 'VisitorController@product_show');
Route::post('add_product_cart', 'VisitorController@add_product_cart');
Route::get('cart', 'VisitorController@cart');
Route::post('cart_show', 'VisitorController@cart_show');
Route::post('cart_deletes', 'VisitorController@cart_delete');
Route::post('cart_checkout', 'VisitorController@cart_checkout');
Route::get('order', 'VisitorController@order');
Route::post('order_show', 'VisitorController@order_show');


//Auth是laravel裡面內建的註冊以及登入還有一些有的沒的
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/products', 'HomeController@product');
Route::post('/newss', 'HomeController@news');
            // prefix後面接的是127.0.0.1/shoppingsystem
            // namespace後面是接controller的資料夾

Route::group(['prefix' => 'shoppingsystem','namespace' => 'Shopping'], function () {
    Route::get('index', 'ShoppingController@index');   //首頁顯示資訊
        Route::post('product', 'ShoppingController@product'); //顯示商品項目
        Route::post('news', 'ShoppingController@news');    //顯示消息項目
        Route::post('member', 'ShoppingController@member');  //顯示要會員項目
        Route::post('manager', 'ShoppingController@manager'); //顯示要管理員項目
        Route::get('login', 'ShoppingController@login');   //登入頁面顯示
        Route::post('logins', 'ShoppingController@logins');   //登入導向登入控制器

    Route::group(['prefix' => 'news'], function () {
        Route::get('index', 'NewsController@index');   //首頁
        Route::post('show', 'NewsController@show');    //顯示所有消息
        Route::post('add', 'NewsController@store');   //送出要新增消息
        Route::post('edit', 'NewsController@update');  //送出要修改消息
        Route::post('delete', 'NewsController@destroy'); //摧毀不要的消息
    });
    Route::group(['prefix' => 'member'], function () {
        Route::get('index', 'MemberController@index');   //首頁
        Route::post('show', 'MemberController@show');    //顯示要新增會員
        Route::post('shows', 'MemberController@shows');    //顯示要新增管理員
        Route::post('add', 'MemberController@store');   //送出要新增會員
        Route::post('adds', 'MemberController@stores');   //送出要新增管理員
        Route::post('edit', 'MemberController@update');  //送出要修改會員
        Route::post('edits', 'MemberController@updates');  //送出要修改管理員
        Route::post('delete', 'MemberController@destroy'); //摧毀不要會員
        Route::post('deletes', 'MemberController@destroys'); //摧毀不要管理員
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('index', 'ProductController@index');   //首頁
        Route::post('show', 'ProductController@show');    //顯示所有商品
        Route::post('add', 'ProductController@store');   //送出要新增商品
        Route::post('edit', 'ProductController@update');  //送出要修改商品
        Route::post('delete', 'ProductController@destroy'); //摧毀不要的商品
    });
    Route::group(['prefix' => 'orderlist'], function () {
        Route::get('index', 'OrderlistController@index');   //index顯示資訊
        Route::post('show', 'OrderlistController@show');    //顯示要新增的項目
        Route::post('delete', 'OrderlistController@destroy'); //摧毀不要的資料
    });
});
