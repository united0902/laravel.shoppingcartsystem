<?php

namespace App;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users'; //此為資料表名稱
    public $timestamps = true;  //是否要紀錄時戳
    /**
     * The attributes that are mass assignable.
     * 可批量分配的屬性
     * @var array
     */
    protected $fillable = [
        'account',
        'password',
        'name',
        'sex',
        'email',
        'phone',
        'adds',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 應該為數組隱藏的屬性
     * @var array
     */

    //hidden為隱藏欄位
    //請求令牌：https://laravel.com/docs/5.5/passport#requesting-password-grant-tokens
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
