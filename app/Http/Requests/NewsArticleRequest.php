<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'title'    =>   'required|string|min:3|unique:news',
            'keyword'  =>   'required|string|min:3|unique:news',
            'content'  =>   'required|string|min:3|unique:news'

        ];
    }
    public function messages()
    {
        return [
            
            '*.required' => ':attribute ' . '不得為空',
            '*.string'   => ':attribute ' . '要為字串',
            '*.min'      => ':attribute ' . '至少輸入3個字',
            '*.unique'   => ':attribute ' . '不得重複',
        ];
    }

    public function attributes()
    {
        return [

            'title'    =>  '標題',
            'keyword'  =>  '關鍵字' ,
            'content'  =>  '內容',

        ];
    }
}
