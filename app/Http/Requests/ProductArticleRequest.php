<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'title'    =>   'required|string|',
            'price'    =>   'required|int|',
            'enough'   =>   'required|int|',
            'file'     =>   'required|file|mimes:jpeg,png,gif',
            'content'  =>   'required|string|min:3'

        ];
    }
    public function messages()
    {
        return [
            
            '*.required'       => ':attribute ' . '不得為空',
            'title.string'     => ':attribute ' . '要為字串',
            'content.string'   => ':attribute ' . '要為字串',
            'price.int'        => ':attribute ' . '要為整數',
            'enough.int'       => ':attribute ' . '要為整數',
            'file.file'        => ':attribute ' . '要為檔案',
            'file.mimes'       => ':attribute ' . '要為圖檔',
            'file.unique'      => ':attribute ' . '不得重複',
            'content.min'      => ':attribute ' . '至少輸入3個字',
        ];
    }

    public function attributes()
    {
        return [

            'title'    =>  '商品名稱' ,
            'file'     =>  '商品照片' ,
            'price'    =>  '商品價格' ,
            'enough'   =>  '商品庫存' ,
            'content'  =>  '商品內容' ,

        ];
    }
}
