<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagerArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'account'  => 'required|string|max:50|unique:managers',
            'password' => 'required|string|min:6|confirmed',
            'name'     => 'required|string|max:20',
            'email'    => 'required|string|email|max:255|unique:managers',
            'phone'    => 'required|string|max:20|min:7',
            'adds'     => 'required|string|max:255',

        ];
    }
    public function messages()
    {
        return [
            
            'account.max'         => ':attribute ' . '最多輸入50個字元',
            'name.max'            => ':attribute ' . '最多輸入20個字元',
            'email.max'           => ':attribute ' . '最多輸入255個字元',
            'phone.max'           => ':attribute ' . '最多輸入20個字元',
            'phone.min'           => ':attribute ' . '最多輸入7個字元',
            'adds.max'            => ':attribute ' . '最多輸入255個字元',
            'password.confirmed'  => ':attribute ' . '請正確鍵入您的密碼',
            'email.email'         => ':attribute ' . '請正確鍵入E-mail格式',
            '*.required'          => ':attribute ' . '不得為空',
            '*.string'            => ':attribute ' . '要為字串',
            '*.min'               => ':attribute ' . '至少輸入1個字',
            '*.unique'            => ':attribute ' . '不得重複',

        ];
    }

    public function attributes()
    {
        return [

            'account'    =>  '管理員帳號',
            'password'   =>  '管理員密碼',
            'name'       =>  '管理員姓名',
            'adds'       =>  '管理員地址',
            'email'      =>  '管理員信箱',
            'phone'      =>  '管理員手機',

        ];
    }
}
