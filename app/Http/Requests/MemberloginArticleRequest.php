<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberloginArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account'  => 'required|string',
            'password' => 'required|string',

        ];
    }
    public function messages()
    {
        return [
            
            '*.required'          => ':attribute ' . '不得為空',
            '*.string'            => ':attribute ' . '要為字串',
            '*.unique'            => ':attribute ' . '不得重複',

        ];
    }

    public function attributes()
    {
        return [

            'account'    =>  '帳號',
            'password'   =>  '密碼' ,

        ];
    }
}
