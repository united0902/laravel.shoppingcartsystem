<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersupdatesArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'account'  => 'required|string|max:50',
            'password' => 'required|string|min:6|confirmed',
            'name'     => 'required|string|max:20',
            'sex'      => 'required|string|max:5',
            'email'    => 'required|string|email|max:255',
            'phone'    => 'required|string|max:20|min:7',
            'adds'     => 'required|string|max:255',

        ];
    }
    public function messages()
    {
        return [
            
            'account.max'         => ':attribute ' . '最多輸入50個字元唷',
            'name.max'            => ':attribute ' . '最多輸入20個字元',
            'email.max'           => ':attribute ' . '最多輸入255個字元',
            'phone.max'           => ':attribute ' . '最多輸入20個字元',
            'phone.min'           => ':attribute ' . '最少輸入7個字元',
            'adds.max'            => ':attribute ' . '最多輸入255個字元',
            'password.confirmed'  => ':attribute ' . '請確認是否正確',
            'email.email'         => ':attribute ' . '請正確鍵入E-mail格式',
            '*.required'          => ':attribute ' . '不得為空',
            '*.string'            => ':attribute ' . '要為字串',
            '*.min'               => ':attribute ' . '至少輸入:min個字',

        ];
    }

    public function attributes()
    {
        return [

            'account'    =>  '會員帳號',
            'password'   =>  '會員密碼' ,
            'name'       =>  '會員姓名',
            'sex'        =>  '會員性別',
            'adds'       =>  '會員居住地址',
            'email'      =>  '會員電子信箱',
            'phone'      =>  '會員手機電話',

        ];
    }
}
