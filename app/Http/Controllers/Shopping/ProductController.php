<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductArticleRequest;

use App\Model\Product as model_product;

class ProductController extends Controller
{
    public function index()
    {
        return view('manager.product.index');
    }
    public function store(ProductArticleRequest $request)
    {
        $title = $request->title;
        $price = $request->price;
        $enough = $request->enough;
        $content = $request->content;
        $file = $request->file;
        // 取得上傳檔案的副檔名
        $extension = $file->getClientOriginalExtension();
        // 將檔案名重新命名為時間戳+亂數
        $filename = time().rand(0, 10000).'.'.$extension;
        // 取得public目錄的完整路徑,將照片存入相對路徑
        $filepath = public_path('images');
        // 移動上傳的檔案到對應路徑
        $file->move($filepath, $filename);

        $product = new model_product;
        $product->title = $title;
        $product->price = $price;
        $product->enough = $enough;
        $product->content = $content;
        $product->filename = $filename;
        $product->filetype = $extension;
        $result = $product->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );

        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $product->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function show(Request $request)
    {
        $title = $request->title;
        $price = $request->price;
        $enough = $request->enough;
        $content = $request->content;

        $model = new model_product();

        if ($title != '') {
            $select =  $model::where('title', 'like', "%$title%")->get();
        } elseif ($price != '') {
            $select =  $model::where('price', 'like', "%$price%")->get();
        } elseif ($enough != '') {
            $select =  $model::where('enough', 'like', "%$enough%")->get();
        } elseif ($content != '') {
            $select =  $model::where('content', 'like', "%$content%")->get();
        } else {
            $select = $model::all();
        }

        return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $select]);
    }
    public function update(Request $request)
    {
        if ($request->hasFile('file') == false) {
            $title = $request->title;
            $price = $request->price;
            $enough = $request->enough;
            $content = $request->content;

            $model = new model_product();
            $model = $model->where('_id', $request->input('id'))
                        ->first();
            $model->title = $title;
            $model->price = $price;
            $model->enough = $enough;
            $model->content = $content;
            $result = $model->save();

            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );


            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $model->_id;
                $response['message'] = 'Success';
            }

            return response()->json($response, 200);
        } else {
            $title = $request->title;
            $price = $request->price;
            $enough = $request->enough;
            $content = $request->content;
            $file = $request->file;
            //取得上傳檔案的副檔名
            $extension = $file->getClientOriginalExtension();
            //將檔案名重新命名
            $filename = time().rand(0, 10000).'.'.$extension;
            //取得public目錄的完整路徑,將照片存入相對路徑
            $filepath = public_path('images');
            //移動上傳的檔案到對應路徑
            $file->move($filepath, $filename);
    
            $model = new model_product();
            $model = $model->where('_id', $request->input('id'))
                           ->first();
            $model->title = $title;
            $model->price = $price;
            $model->enough = $enough;
            $model->content = $content;
            $model->filename = $filename;
            $model->filetype = $extension;
            $result = $model->save();
    
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );
    
    
            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $model->_id;
                $response['message'] = 'Success';
            }
    
            return response()->json($response, 200);
        }
    }
    public function destroy(Request $request)
    {
        $model = new model_product();
        $result = $model->where('_id', $request->input('id'))
                       ->delete();

        $response = array(
        'code' => '0',
        'value' => '',
        'message' => ''
        );

        if ($result) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        } else {
            $response['code'] = '0';
            $response['value'] = $model->_id;
            $response['message'] = 'false';
        }
        return response()->json($response, 200);
    }
}
