<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Order as model_order;

class OrderlistController extends Controller
{
    public function index()
    {
        return view('manager.orderlist.index');
    }
    public function show(Request $request)
    {
        $request->order;

        if ($request->order === "true") {
            $orders = model_order::all();

            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $orders]);
        }
    }
    public function destroy(Request $request)
    {
        $model = new model_order();
        $result = $model->where('_id', $request->input('id'))->delete();

        $response = array(
        'code' => '0',
        'value' => '',
        'message' => ''
        );

        if ($result) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        } else {
            $response['code'] = '0';
            $response['value'] = $model->_id;
            $response['message'] = 'false';
        }
        return response()->json($response, 200);
    }
}
