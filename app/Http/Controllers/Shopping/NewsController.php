<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsArticleRequest;

use App\Model\News as model_news;

class NewsController extends Controller
{
    public function index()
    {
        return view('manager.news.index');
    }
    public function store(NewsArticleRequest $request)
    {
        $model = new model_news();
        $model->title = $request->get('title');
        $model->keyword = $request->get('keyword');
        $model->content = $request->get('content');
        $result = $model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );

        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function show(Request $request)
    {
        $title = $request->title;
        $keyword = $request->keyword;
        $content = $request->content;

        $model = new model_news();

        if ($title != '') {
            $select =  $model::where('title', 'like', "%$title%")->get();
        } elseif ($keyword != '') {
            $select =  $model::where('keyword', 'like', "%$keyword%")->get();
        } elseif ($content != '') {
            $select =  $model::where('content', 'like', "%$content%")->get();
        } else {
            $select = $model::all();
        }

        return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $select]);
    }
    public function update(Request $request)
    {
        $model = new model_news();
        $model = $model->where('_id', $request->input('id'))
                       ->first();

        $model->title = $request->get('title');
        $model->keyword = $request->get('keyword');
        $model->content = $request->get('content');
        $result = $model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );

        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function destroy(Request $request)
    {
        $model = new model_news();
        $result = $model->where('_id', $request->input('id'))
                       ->delete();

        $response = array(
        'code' => '0',
        'value' => '',
        'message' => ''
        );

        if ($result) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        } else {
            $response['code'] = '0';
            $response['value'] = $model->_id;
            $response['message'] = 'false';
        }
        return response()->json($response, 200);
    }
}
