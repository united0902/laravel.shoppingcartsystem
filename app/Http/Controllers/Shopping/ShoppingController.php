<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\Model\News as model_news;
use App\Model\Member as model_member;
use App\Model\Manager as model_manager;
use App\Model\Product as model_product;
use App\Http\Requests\MemberloginArticleRequest;

class ShoppingController extends Controller
{
    public function index()
    {
        return view('manager.index');
    }
    public function login()
    {
        return view('manager.users.logins');
    }
    public function logins(MemberloginArticleRequest $request)
    {
        $account = $request->account;
        $password = $request->password;


        $model = new model_manager;
        $managers = $model::all()->where('account', $account)->first();

        //使用 Auth 類別來做驗證。如果驗證成功，Auth::attempt() 方法會回傳 true，這時就可以把它導向內部頁面.
        if (Auth::attempt(['account' => $account, 'password' => $password])) {
            Session::put(['account' => $account]);

            $result = true;
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );
    
    
            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $managers->_id;
                $response['message'] = 'Success';
            }
    
            return response()->json($response, 200);
        } else {
            $errors = "False";
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );
    
    
            if ($errors === "False") {
                $response['code'] = '1';
                $response['value'] = '0';
                $response['message'] = 'False';
            }
    
            return response()->json($response, 200);
        }
    }
    public function product(Request $request)
    {
        $request->product;

        if ($request->product === "true") {
            $products = model_product::all();


            $json = json_encode($products, JSON_UNESCAPED_UNICODE);
        
            return $json;
        }
    }
    public function news(Request $request)
    {
        $request->news;

        if ($request->news === "true") {
            $newss = model_news::all();


            $json = json_encode($newss, JSON_UNESCAPED_UNICODE);
        
            return $json;
        }
    }
    public function member(Request $request)
    {
        $request->member;

        if ($request->member === "true") {
            $members = model_member::all();


            $json = json_encode($members, JSON_UNESCAPED_UNICODE);
        
            return $json;
        }
    }
    public function manager(Request $request)
    {
        $request->manager;

        if ($request->manager === "true") {
            $managers = model_manager::all();


            $json = json_encode($managers, JSON_UNESCAPED_UNICODE);
        
            return $json;
        }
    }
}
