<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MemberArticleRequest;
use App\Http\Requests\ManagerArticleRequest;
use App\Model\Member as model_member;
use App\Model\Manager as model_manager;

class MemberController extends Controller
{
    public function index()
    {
        return view('manager.member.index');
    }
    public function store(MemberArticleRequest $request)
    {
        $model = new model_member();
        $model->account  = $request->get('account');
        $model->password = bcrypt($request->get('password'));
        $model->name     = $request->get('name');
        $model->sex      = $request->get('sex');
        $model->adds     = $request->get('adds');
        $model->email    = $request->get('email');
        $model->phone    = $request->get('phone');
        $result = $model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );

        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function show(Request $request)
    {
        $account = $request->account;
        $name = $request->name;
        $email = $request->email;
        $adds = $request->adds;
        $phone = $request->phone;

        $model = new model_member();

        if ($account != '') {
            $select =  $model::where('account', 'like', "%$account%")->get();
        } elseif ($name != '') {
            $select =  $model::where('name', 'like', "%$name%")->get();
        } elseif ($email != '') {
            $select =  $model::where('email', 'like', "%$email%")->get();
        } elseif ($adds != '') {
            $select =  $model::where('adds', 'like', "%$adds%")->get();
        } elseif ($phone != '') {
            $select =  $model::where('phone', 'like', "%$phone%")->get();
        } else {
            $select = $model::all();
        }
       
        return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $select]);
    }
    public function update(Request $request)
    {
        $model = new model_member();
        $model = $model->where('_id', $request->input('id'))
                        ->first();

        $model->account  = $request->get('account');
        $model->name     = $request->get('name');
        $model->sex      = $request->get('sex');
        $model->adds     = $request->get('adds');
        $model->email    = $request->get('email');
        $model->phone    = $request->get('phone');
        $result = $model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );

        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function destroy(Request $request)
    {
        $model = new model_member();
        $result = $model->where('_id', $request->input('id'))
                       ->delete();

        $response = array(
        'code' => '0',
        'value' => '',
        'message' => ''
        );

        if ($result) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        } else {
            $response['code'] = '0';
            $response['value'] = $model->_id;
            $response['message'] = 'false';
        }
        return response()->json($response, 200);
    }
    public function stores(ManagerArticleRequest $request)
    {
        $model = new model_manager();
        $model->account  = $request->get('account');
        $model->password = bcrypt($request->get('password'));
        $model->name     = $request->get('name');
        $model->adds     = $request->get('adds');
        $model->email    = $request->get('email');
        $model->phone    = $request->get('phone');
        $result = $model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );


        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function shows(Request $request)
    {
        $account = $request->account;
        $name = $request->name;
        $email = $request->email;
        $adds = $request->adds;
        $phone = $request->phone;

        $model = new model_manager();

        if ($account != '') {
            $select =  $model::where('account', 'like', "%$account%")->get();
        } elseif ($name != '') {
            $select =  $model::where('name', 'like', "%$name%")->get();
        } elseif ($email != '') {
            $select =  $model::where('email', 'like', "%$email%")->get();
        } elseif ($adds != '') {
            $select =  $model::where('adds', 'like', "%$adds%")->get();
        } elseif ($phone != '') {
            $select =  $model::where('phone', 'like', "%$phone%")->get();
        } else {
            $select = $model::all();
        }
       

        return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $select]);
    }
    public function updates(Request $request)
    {
        $model = new model_manager();
        $model = $model->where('_id', $request->input('id'))
                        ->first();
        $model->account  = $request->get('account');
        $model->name     = $request->get('name');
        $model->adds     = $request->get('adds');
        $model->email    = $request->get('email');
        $model->phone    = $request->get('phone');
        $result = $model->save();
        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );


        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
    public function destroys(Request $request)
    {
        $model = new model_manager();
        $result = $model->where('_id', $request->input('id'))
                       ->delete();

        $response = array(
        'code' => '0',
        'value' => '',
        'message' => ''
        );

        if ($result) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        } else {
            $response['code'] = '0';
            $response['value'] = $model->_id;
            $response['message'] = 'false';
        }
        return response()->json($response, 200);
    }
}
