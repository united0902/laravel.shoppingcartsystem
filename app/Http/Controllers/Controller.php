<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // if(Auth::check() == false){
        //     return Redirect::guest('login');
        // }
    }
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    // protected function setupLayout()
    // {
    //     if ( ! is_null($this->layout))
    //     {
    //         $this->layout = View::make($this->layout);
    //     }
    // }
}
