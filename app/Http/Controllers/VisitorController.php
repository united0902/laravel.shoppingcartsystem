<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\News;
use App\Model\Member;
use App\Model\Product;
use App\Model\Order;
use App\Http\Requests\UsersupdatesArticleRequest;

class VisitorController extends Controller
{
    public function news()
    {
        return view('news');
    }
    public function users()
    {
        return view('users');
    }
    public function product()
    {
        return view('product');
    }
    public function order()
    {
        return view('order');
    }
    public function cart()
    {
        return view('cart');
    }
    /**
     * 取得會員訂購資料.
     *
     * @param [string] Ajax傳近資料 [true]
     *
     * @return [Collection] [詳細資料]
     */
    public function order_show(Request $request)
    {
        // 取得陣列數值
        $sMessage = $request->order;
        // 初始化陣列
        $aParam = array();
        // 取得當前使用者帳號
        $user = Session::get('account');
        // 將目前登入人員存入陣列
        $aParam = [
            'users' => $user,
        ];
        // 判斷是否有回應
        if ($sMessage) {
            // 取得資料庫內容
            $model = new Order;
            // 初始化陣列
            $aWhere = array('users',);
            foreach ($aWhere as $sVal) {
                if (isset($aParam[$sVal]) && $aParam[$sVal] != '') {
                    $_type = gettype($aParam[$sVal]);
                    if ($sVal === 'users' && $_type === 'string') {
                        $model = $model->where($sVal, $aParam[$sVal]);
                    }
                }
            }
            $result = $model->get();

            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $result]);
        }
    }

    public function news_show(Request $request)
    {
        $request->news;

        if ($request->news === "true") {
            $newss = News::all();

            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $newss]);
        }
    }
    public function users_show()
    {
        //獲得session值
        $account = Session::get('account');
        $success=true;

        if ($success == true) {
            $model = new Member();
            $models = $model::all()->where("account", $account)->first();

            $result = $models;

            $response = array(
                'code' => '666',
                'value' => $result,
                'message' => 'Success'
            );
    
            return response()->json($response, 200);
        } else {
            return "False";
        }
    }
    public function users_update(UsersupdatesArticleRequest $request)
    {
        $account = $request->account;
        $password = bcrypt(($request->password));
        $name = $request->name;
        $sex = $request->sex;
        $email = $request->email;
        $phone = $request->phone;

        $model = new Member();
        $users = $model::where('account', $account)->first();

        $success = true;
        if ($success = true) {
            $users->password = $password;
            $users->name = $name;
            $users->sex = $sex;
            $users->email = $email;
            $users->phone = $phone;
            $update = $users->save();

            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );
    
    
            if ($update === true) {
                $response['code'] = '1';
                $response['value'] = $users->name;
                $response['message'] = 'Success';
            }
    
            return response()->json($response, 200);
        } else {
            return "False";
        }
    }
    public function product_show(Request $request)
    {
        $request->product;

        if ($request->product === "true") {
            $products = Product::all();
            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $products]);
        }
    }
    public function add_product_cart(Request $request)
    {
        $id = $request->input("id");
        $model = new Product();
        $cart = $model::where("_id", $id)->first();
       
        if ($request->session()->has('shoppingcart') == false) {
            $carts = array();
            $request->session()->put('shoppingcart', $carts);

            $result = true;
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );


            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $carts;
                $response['message'] = 'Success';
            }

            return response()->json($response, 200);
        }
        
        $carts = array();
        if ($request->session()->has('shoppingcart') == true) {
            $carts = $request->session()->get('shoppingcart');

            foreach ($carts as $val) {
                if ($val["_id"] == $cart["_id"]) {
                    $response = array(
                        'code' => '0',
                        'value' => $cart,
                        'message' => 'False'
                    );
                    return response()->json($response, 200);
                }
            }
            $carts[] = $cart;

            $request->session()->put('shoppingcart', $carts);


            $result = true;
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );


            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $carts;
                $response['message'] = 'Success';
            }

            return response()->json($response, 200);
        } else {
            return "Error";
        }
    }
    public function cart_show(Request $request)
    {
        $request->cart;

        if ($request->cart === "true") {
            $cart = Session::get('shoppingcart');

            $response = array(
                'retCode' => 1,
                'retVal'  => $cart,
                'retMsg'  => 'success'
            );

            return response()->json($response);
        }
    }
    public function cart_delete(Request $request)
    {
        $id = $request->input("id");
        $model = new Product();
        $cart = $model::where("_id", $id)->first()->toArray();

        $carts = array();
        if ($request->session()->has('shoppingcart') == true) {
            $carts = $request->session()->get('shoppingcart');

            foreach ($carts as $key => $val) {
                if ($val["_id"] == $cart["_id"]) {
                    unset($carts[$key]);
                    
                    $request->session()->put('shoppingcart', $carts);
                    
                    $result = true;
                    $response = array(
                        'code' => '0',
                        'value' => '',
                        'message' => ''
                    );


                    if ($result === true) {
                        $response['code'] = '1';
                        $response['value'] = $carts;
                        $response['message'] = 'Success';
                    }

                    return response()->json($response, 200);
                }
            }
        } else {
            return "Error";
        }
    }
    public function cart_checkout(Request $request)
    {
        $account = $request->session()->has('account');

        if ($request->session()->has('account') == true) {
            $order = $request->session()->get('shoppingcart');
            $product_much = count($order); //從session讀取購物筆數

            $orders_id = time().rand(0, 1000000);//隨機0到百萬訂單編號

            if ($product_much == 0) {
                $response = array(
                    'code' => '2',
                    'value' => 'Error',
                    'message' => 'PLZBuySomething'
                );
                return response()->json($response, 200);
            }

            for ($a=0; $a<$product_much; $a++) {
                $id = $order[$a]["_id"];
                $title = $order[$a]["title"];
                $price = $order[$a]["price"];
                $enough = $request->enough[$a];

                $users = Session::get('account');

                $model = new Order;
                $model->product_id = $id;
                $model->title = $title;
                $model->price = $price;
                $model->enough = $enough;
                $model->users = $users;
                //將訂單編號轉為string
                $model->orders_id =(string) $orders_id;
                $result = $model->save();
            }
        }
        if ($result == true) {
            $request->session()->put('shoppingcart', []);
            $response = array(
                    'code' => '0',
                    'value' => '1',
                    'message' => 'Success'
                );
            return response()->json($response, 200);
        } else {
            $response = array(
                'code' => '1',
                'value' => 'False',
                'message' => 'Error'
            );
            return response()->json($response, 200);
        }
    }
}
