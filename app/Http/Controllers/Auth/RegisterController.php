<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\MemberArticleRequest;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function register(MemberArticleRequest $request)
    {
        $account  = $request->get('account');
        $password = bcrypt($request->get('password'));
        $name     = $request->get('name');
        $sex      = $request->get('sex');
        $email    = $request->get('email');
        $phone    = $request->get('phone');
        $adds     = $request->get('adds');

        $model = new User;
        $model->account = $account;
        $model->password = $password;
        $model->name = $name;
        $model->sex = $sex;
        $model->email = $email;
        $model->phone = $phone;
        $model->adds = $adds;
        $result=$model->save();

        $response = array(
            'code' => '0',
            'value' => '',
            'message' => ''
        );


        if ($result === true) {
            $response['code'] = '1';
            $response['value'] = $model->_id;
            $response['message'] = 'Success';
        }

        return response()->json($response, 200);
    }
}
