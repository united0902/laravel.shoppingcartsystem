<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\MemberloginArticleRequest;
use Auth;
use Session;
use App\User as model_users;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(MemberloginArticleRequest $request)
    {
        $account = $request->account;
        $password = $request->password;

        $model = new model_users;
        $users = $model::all()->where('account', $account)->first();

        //使用 Auth 類別來做驗證。如果驗證成功，Auth::attempt() 方法會回傳 true，這時就可以把它導向內部頁面.
        if (Auth::attempt(['account' => $account, 'password' => $password])) {
            Session::put(['account' => $account]);
            $result = true;
            $response = array(
                'code' => '0',
                'value' => '',
                'message' => ''
            );
    
    
            if ($result === true) {
                $response['code'] = '1';
                $response['value'] = $users->_id;
                $response['message'] = 'Success';
            }
    
            return response()->json($response, 200);
        } else {
            $errors = "False";
            $response = array(
                    'code' => '0',
                    'value' => '',
                    'message' => ''
                );

            if ($errors === "False") {
                $response['code'] = '1';
                $response['value'] = '0';
                $response['message'] = 'False';
            }
        
            return response()->json($response, 200);
        }
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //除了 logout 方法以外一律套用 guest 中介層
    }
}
