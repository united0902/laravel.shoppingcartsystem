<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\News as model_news;
use App\Model\Product as model_product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); //驗證有沒有登入
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function product(Request $request)
    {
        $request->product;

        if ($request->product === "true") {
            $products = model_product::all();

            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $products]);
        }
    }
    public function news(Request $request)
    {
        $request->news;

        if ($request->news === "true") {
            $newss = model_news::all();

            return response()->json(['retCode' => 1, 'retMsg' => 'success','retVal' => $newss]);
        }
    }
}
