<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *可批量分配的屬性
     * @var array
     */

    //這邊是允許寫入的欄位
    protected $fillable = [
        'title',
        'keyword',
        'content',
    ];
}
