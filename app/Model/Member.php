<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model;

class Member extends Model
{
    protected $table = 'users';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *可批量分配的屬性
     * @var array
     */
    protected $fillable = [
        'account',
        'password',
        'name',
        'sex',
        'email',
        'phone',
        'adds',
    ];//這邊是允許寫入的欄位

    /**
    * The attributes that should be hidden for arrays.
    * 應該為數組隱藏的屬性
    * @var array
    */

    //hidden為隱藏欄位
    //請求令牌：https://laravel.com/docs/5.5/passport#requesting-password-grant-tokens
    protected $hidden = [
        'password',
        'remember_token',

    ];
}
