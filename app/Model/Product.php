<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //這邊是允許寫入的欄位
    protected $fillable = [
        'id',
        'title',
        'image',
        'file',
        'filename',
        'filetype',
        'price',
        'content',
    ];
}
