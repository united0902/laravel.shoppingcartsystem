<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orderlist';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //這邊是允許寫入的欄位
    protected $fillable = [
        'product_id',
        'title',
        'price',
        'enough',
        'user',
        'orders_id'
    ];
}
