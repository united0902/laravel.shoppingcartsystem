<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model;

class Manager extends Model
{
    protected $table = 'managers';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *可批量分配的屬性
     * @var array
     */

    //這邊是允許寫入的欄位
    protected $fillable = [
        'account',
        'password',
        'name',
        'adds',//這邊是允許寫入的欄位
        'email',
        'phone',
    ];
}
