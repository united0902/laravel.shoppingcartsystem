$('#manager_login').submit(function (event) {
    event.preventDefault();//不轉頁
    // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
    var data = {
        'account': $('#account').val(),
        'password': $('#password').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/logins', //127.0.0.1:8000/shoppingsystem/logins
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("登錄成功");
                setTimeout("window.location.href='index'", 1500);
            }
            if (result.message === "False") {
                toastr.error("登錄失敗.請重新登錄");
                setTimeout("window.location.href='login'", 1500);
            }
        },
    });
});