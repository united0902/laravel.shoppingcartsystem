
// 下面是新增消息
CKEDITOR.replace('content', {});
CKEDITOR.replace('editcontent', {});
$('#create').submit(function (event) {
    event.preventDefault();//不轉頁
    // var data = $(this).serialize(); //宣告data為上方create所接收的所有欄位值
    var data = {
        'title': $('#title').val(),
        'keyword': $('#keyword').val(),
        'content': CKEDITOR.instances.content.getData()
    }
    $.ajax({
        url: base_url + '/shoppingsystem/news/add', //127.0.0.1:8000/shoppingsystem/news/add
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {

            if (result.message === "Success") {
                toastr.success("新增成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + "<br>"; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                //彈出視窗跟用戶講哪個欄位錯誤.
                toastr.error(error_msg);
            }
        }
    });

});

// 下面是顯示所有消息資料
$(function () {
    $("#select").click(function () {
        var data = {
            'title': $('#news_title').val(),
            'keyword': $('#news_keyword').val(),
            'content': $('#news_content').val(),
        }
        $.ajax({
            type: 'post',
            url: base_url + '/shoppingsystem/news/show',
            data: data,
            dataType: 'json',
            //避免跨站偽造請求攻擊
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (show) {
                let html = '';
                if (typeof (show.retVal) === 'object') {
                    for (let i in show.retVal) {
                        let value = show.retVal[i];
                        html += '<tr>';
                        html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                        html += '<td class="text-center">' + value.title + '</td>';
                        html += '<td class="text-center">' + value.keyword + '</td>';
                        html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '">' + value.content + '</td>';
                        html += '<td class="text-center"><button class="btn btn-warning" type="button" data-toggle="modal" data-target="#editnews" onclick="edit()">修改</button></td>';
                        html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#delnews" onclick="del()">刪除</button></td>';
                        html += '</tr>'
                    }
                }
                $('#show').html(html);
            },
        })

    })
});

// 下面是消息修改
function edit() {
    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");
    $('#edittitle').val("");
    $('#editkeyword').val("");
    $('#editcontent').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));
    $('#edittitle').val($(event_td).eq(1).html());
    $('#editkeyword').val($(event_td).eq(2).html());
    CKEDITOR.instances['editcontent'].setData($(event_td).eq(3).data('content'));
}

$('#editnews').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'title': $('#edittitle').val(),
        'keyword': $('#editkeyword').val(),
        'content': CKEDITOR.instances.editcontent.getData()
    }

    $.ajax({
        url: base_url + '/shoppingsystem/news/edit', //127.0.0.1:8000/shoppingsystem/news/update
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("修改成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
            }
        }

    });

});

// 下面是消息刪除
function del() {

    let event_src = this.event.srcElement;
    let event_tr = $(event_src).parent().parent();
    let event_td = $(event_tr).find('td');

    $('#id').val("");
    $('#deltitle').val("");
    $('#delkeyword').val("");
    $('#delcontent').val("");

    $('#id').val($(event_td).eq(0).data('id'));
    $('#deltitle').val($(event_td).eq(1).html());
    $('#delkeyword').val($(event_td).eq(2).html());
    $('#delcontent').val($(event_td).eq(3).data('content'));
}

$('#delnews').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'title': $('#deltitle').val(),
        'keyword': $('#delkeyword').val(),
        'content': $('#delcontent').val(),
    }

    $.ajax({
        url: base_url + '/shoppingsystem/news/delete', //127.0.0.1:8000/shoppingsystem/news/delete
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("刪除成功");
                setTimeout("window.location.href='index'", 1500);
            }

        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
            }
        }
    });
});
