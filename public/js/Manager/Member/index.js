// 下面是新增會員
$('#createusers').submit(function (event) {
    event.preventDefault();//不轉頁

    // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
    var data = {
        'account': $('#account').val(),
        'password': $('#password').val(),
        'password_confirmation': $('#password_confirmation').val(),
        'name': $('#name').val(),
        'sex': $('#sex').val(),
        'adds': $('#adds').val(),
        'email': $('#email').val(),
        'phone': $('#phone').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/member/add', //127.0.0.1:8000/shoppingsystem/member/add
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {

            if (result.message === "Success") {
                toastr.success("新增成功");
                setTimeout("window.location.href='index'", 1500);

            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + "<br>"; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                toastr.error(error_msg);
            }
        }
    });
});

// 下面是顯示所有會員資料
$(function () {
    $("#select").click(function () {
        var data = {
            'account': $('#users_account').val(),
            'name': $('#users_name').val(),
            'email': $('#users_email').val(),
            'adds': $('#users_adds').val(),
            'phone': $('#users_phone').val(),
        }
        $.ajax({
            type: 'post',
            url: base_url + '/shoppingsystem/member/show',
            data: data,
            dataType: 'json',
            //避免跨站偽造請求攻擊
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (show) {
                let html = '';
                if (typeof (show.retVal) === 'object') {
                    for (let i in show.retVal) {
                        let value = show.retVal[i];
                        html += '<tr>';
                        html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                        html += '<td class="text-center">' + value.account + '</td>';
                        html += '<td class="text-center">' + value.name + '</td>';
                        html += '<td class="text-center">' + value.sex + '</td>';
                        html += '<td class="text-center">' + value.email + '</td>';
                        html += '<td class="text-center">' + value.adds + '</td>';
                        html += '<td class="text-center">' + value.phone + '</td>';
                        html += '<td class="text-center"><button class="btn btn-warning" type="button" data-toggle="modal" data-target="#editmember" onclick="edit()">修改</button></td>';
                        html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#del_member" onclick="del()">刪除</button></td>';
                        html += '</tr>'
                    }
                }
                $('#show').html(html);
            },
        })
    })
});

// 下面是會員修改
function edit() {

    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    $('#id').val($(event_td).eq(0).data('id'));
    $('#edit_member_account').val($(event_td).eq(1).html());
    $('#edit_member_name').val($(event_td).eq(2).html());
    $('#edit_member_sex').val($(event_td).eq(3).html());
    $('#edit_member_email').val($(event_td).eq(4).html());
    $('#edit_member_adds').val($(event_td).eq(5).html());
    $('#edit_member_phone').val($(event_td).eq(6).html());

}

$('#edit_member').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'account': $('#edit_member_account').val(),
        'name': $('#edit_member_name').val(),
        'sex': $('#edit_member_sex').val(),
        'email': $('#edit_member_email').val(),
        'adds': $('#edit_member_adds').val(),
        'phone': $('#edit_member_phone').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/member/edit', //127.0.0.1:8000/shoppingsystem/member/update
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("修改成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
            }
        }
    });
});

// 下面是會員刪除
function del() {

    let event_src = this.event.srcElement;
    let event_tr = $(event_src).parent().parent();
    let event_td = $(event_tr).find('td');

    $('#id').val("");
    $('#del_member_account').val("");
    $('#del_member_password').val("");
    $('#del_member_name').val("");
    $('#del_member_sex').val("");
    $('#del_member_email').val("");
    $('#del_member_adds').val("");
    $('#del_member_phone').val("");

    $('#id').val($(event_td).eq(0).data('id'));
    $('#del_member_account').val($(event_td).eq(1).html());
    $('#del_member_password').val($(event_td).eq(2).html());
    $('#del_member_name').val($(event_td).eq(3).html());
    $('#del_member_sex').val($(event_td).eq(4).html());
    $('#del_member_email').val($(event_td).eq(5).html());
    $('#del_member_adds').val($(event_td).eq(6).html());
    $('#del_member_phone').val($(event_td).eq(7).html());

}

$('#del').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'account': $('#del_member_account').val(),
        'password': $('#del_member_password').val(),
        'name': $('#del_member_name').val(),
        'sex': $('#del_member_sex').val(),
        'email': $('#del_member_email').val(),
        'adds': $('#del_member_adds').val(),
        'phone': $('#del_member_phone').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/member/delete', //127.0.0.1:8000/shoppingsystem/member/delete
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("刪除成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
    });
});

// 下面是新增管理員
$('#manager').submit(function (event) {
    event.preventDefault();//不轉頁
    // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
    var data = {
        'account': $('#manager_account').val(),
        'password': $('#manager_password').val(),
        'password_confirmation': $('#manager_password_confirmation').val(),
        'name': $('#manager_name').val(),
        'adds': $('#manager_adds').val(),
        'email': $('#manager_email').val(),
        'phone': $('#manager_phone').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/member/adds', //127.0.0.1:8000/shoppingsystem/member/add
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("新增成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + "<br>"; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                toastr.error(error_msg);
            }
        }
    });
});


// 下面是顯示所有管理員資料
$(function () {
    $("#selects").click(function () {
        var datas = {
            'account': $('#managers_account').val(),
            'name': $('#managers_name').val(),
            'email': $('#managers_email').val(),
            'adds': $('#managers_adds').val(),
            'phone': $('#managers_phone').val(),
        }
        $.ajax({
            type: 'post',
            url: base_url + '/shoppingsystem/member/shows',
            data: datas,
            dataType: 'json',
            //避免跨站偽造請求攻擊
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (shows) {
                let html = '';
                if (typeof (shows.retVal) === 'object') {
                    for (let m in shows.retVal) {
                        let value = shows.retVal[m];
                        html += '<tr>';
                        html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(m)) + (parseInt(1))) + '</td>';
                        html += '<td class="text-center">' + value.account + '</td>';
                        html += '<td class="text-center">' + value.name + '</td>';
                        html += '<td class="text-center">' + value.email + '</td>';
                        html += '<td class="text-center">' + value.adds + '</td>';
                        html += '<td class="text-center">' + value.phone + '</td>';
                        html += '<td class="text-center"><button class="btn btn-warning" type="button" data-toggle="modal" data-target="#editmanager" onclick="edits()">修改</button></td>';
                        html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#del_manager" onclick="dels()">刪除</button></td>';
                        html += '</tr>'
                    }
                }
                $('#shows').html(html);
            },
        })
    })
});

// 下面是管理員修改
function edits() {

    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td


    $('#id').val($(event_td).eq(0).data('id'));
    $('#edit_manager_account').val($(event_td).eq(1).html());
    $('#edit_manager_name').val($(event_td).eq(2).html());
    $('#edit_manager_email').val($(event_td).eq(3).html());
    $('#edit_manager_adds').val($(event_td).eq(4).html());
    $('#edit_manager_phone').val($(event_td).eq(5).html());
}

$('#edit_manager').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'account': $('#edit_manager_account').val(),
        'name': $('#edit_manager_name').val(),
        'sex': $('#edit_manager_sex').val(),
        'email': $('#edit_manager_email').val(),
        'adds': $('#edit_manager_adds').val(),
        'phone': $('#edit_manager_phone').val(),
    }

    $.ajax({
        url: base_url + '/shoppingsystem/member/edits', //127.0.0.1:8000/shoppingsystem/member/updates
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("修改成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                toastr.error(error_msg);
            }
        }
    });

});

// 下面是管理員刪除
function dels() {

    let event_src = this.event.srcElement;
    let event_tr = $(event_src).parent().parent();
    let event_td = $(event_tr).find('td');

    $('#id').val("");
    $('#del_manager_account').val("");
    $('#del_manager_password').val("");
    $('#del_manager_name').val("");
    $('#del_manager_sex').val("");
    $('#del_manager_email').val("");
    $('#del_manager_adds').val("");
    $('#del_manager_phone').val("");

    $('#id').val($(event_td).eq(0).data('id'));
    $('#del_manager_account').val($(event_td).eq(1).html());
    $('#del_manager_password').val($(event_td).eq(2).html());
    $('#del_manager_name').val($(event_td).eq(3).html());
    $('#del_manager_sex').val($(event_td).eq(4).html());
    $('#del_manager_email').val($(event_td).eq(5).html());
    $('#del_manager_adds').val($(event_td).eq(6).html());
    $('#del_manager_phone').val($(event_td).eq(7).html());

}

$('#dels').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editnews所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
        'account': $('#del_manager_account').val(),
        'password': $('#del_manager_password').val(),
        'name': $('#del_manager_name').val(),
        'sex': $('#del_manager_sex').val(),
        'email': $('#del_manager_email').val(),
        'adds': $('#del_manager_adds').val(),
        'phone': $('#del_manager_phone').val(),
    }

    $.ajax({
        url: base_url + '/shoppingsystem/member/deletes', //127.0.0.1:8000/shoppingsystem/member/delete
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("刪除成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
    });
});
