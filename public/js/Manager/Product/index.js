// 下面是高級文字編輯器CKEDITOR的應用
CKEDITOR.replace('content', {});
CKEDITOR.replace('editcontent', {});

// 下面是商品新增,跟以往的新增不一樣
$('#create').submit(function (event) {
    event.preventDefault();//不轉頁

    var form_data = new FormData();
    form_data.append('title', $("#title").val());
    form_data.append('price', $("#price").val());
    form_data.append('enough', $("#enough").val());
    form_data.append('file', $('#create').find('#image')[0].files[0]);
    form_data.append('content', CKEDITOR.instances.content.getData());

    $.ajax({
        url: base_url + '/shoppingsystem/product/add', //127.0.0.1:8000/shoppingsystem/product/add
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post', //傳輸方式
        data: form_data, //上面data的所有值
        dataType: 'json', //json格式
        processData: false,
        contentType: false,

        success: function (response) {
            if (response.message === "Success") {
                toastr.success("新增成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '<br>'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                toastr.error(error_msg);
            }
        }
    });

});

// 下面是查詢商品
$(function () {
    $("#select").click(function () {
        var data = {
            'title': $('#titles').val(),
            'price': $('#prices').val(),
            'enough': $('#enoughs').val(),
            'content': $('#contents').val(),
        }

        $.ajax({
            type: 'post',
            url: base_url + '/shoppingsystem/product/show',
            data: data,
            dataType: 'json',
            //避免跨站偽造請求攻擊
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (product) {
                let html = '';
                if (typeof (product.retVal) === 'object') {
                    for (let i in product.retVal) {
                        let value = product.retVal[i];
                        html += '<tr>';
                        html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                        html += '<td class="text-center">' + value.title + '</td>';
                        html += '<td class="text-center"><img src="' + url + '/' + value.filename + '" width="100" height="100"></td>';
                        html += '<td class="text-center">' + value.price + '</td>';
                        html += '<td class="text-center">' + value.enough + '</td>';
                        html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '">' + value.content + '</td>';
                        html += '<td class="text-center"><button class="btn btn-warning" type="button" data-toggle="modal" data-target="#editproduct" onclick="edit()">修改</button></td>';
                        html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#delproduct" onclick="del()">刪除</button></td>';
                        html += '</tr>'
                    }
                }
                $('#product').html(html);
            },
        })
    })
});

// 下面是商品修改
function edit() {
    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");
    $('#edittitle').val("");
    $('#picture').attr("src", "");
    $('#editimages').val("");
    $('#editprice').val("");
    $('#editenough').val("");
    $('#editcontent').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));
    $('#edittitle').val($(event_td).eq(1).html());
    $('#picture').attr('src', $(event_td).eq(2).find("img").eq(0).attr('src'));
    $('#editimages').val($(event_td).eq(2).data('img'));
    $('#editprice').val($(event_td).eq(3).html());
    $('#editenough').val($(event_td).eq(4).html());
    CKEDITOR.instances['editcontent'].setData($(event_td).eq(5).data('content'));
}



$('#editproduct').submit(function (event) {
    event.preventDefault();//不轉頁

    var form_data = new FormData();
    form_data.append('id', $("#id").val());
    form_data.append('title', $("#edittitle").val());
    form_data.append('price', $("#editprice").val());
    form_data.append('enough', $("#editenough").val());
    form_data.append('file', $('#edit').find('#editimages')[0].files[0]);
    form_data.append('content', CKEDITOR.instances.editcontent.getData());

    $.ajax({
        url: base_url + '/shoppingsystem/product/edit', //127.0.0.1:8000/shoppingsystem/product/update
        type: 'post', //傳輸方式
        data: form_data, //上面data的所有值
        dataType: 'json', //json格式
        processData: false,
        contentType: false,
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("修改成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
        error: function (xhr, status, error) {

            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + '\n'; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
            }
        }

    });

});

// 下面是商品刪除
function del() {
    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));
}

$('#delproduct').submit(function (event) {
    event.preventDefault();//不轉頁

    //var data = $(this).serialize(); //宣告data為上方editproduct所接收的所有欄位值
    var data = {
        'id': $('#id').val(),
    }

    $.ajax({
        url: base_url + '/shoppingsystem/product/delete', //127.0.0.1:8000/shoppingsystem/product/delete
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("刪除成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
    });
});

