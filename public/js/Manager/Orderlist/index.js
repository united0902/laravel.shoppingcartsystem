
// 下面是顯示訂單資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/shoppingsystem/orderlist/show',
        data: { 'order': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (order) {
            let html = '';
            if (typeof (order.retVal) === 'object') {
                for (let i in order.retVal) {
                    let value = order.retVal[i];
                    html += '<tr>';
                    html += '<td class="text-center">' + value.orders_id + '</td>';
                    html += '<td class="text-center">' + value.users + '</td>';
                    html += '<td class="text-center">' + value.title + '</td>';
                    html += '<td class="text-center">' + value.enough + '</td>';
                    html += '<td class="text-center">' + value.price + '</td>';
                    html += '<td class="text-center">' + (value.enough * value.price) + '</td>';
                    html += '<td class="text-center" style="display:none;" type="hidden" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                    html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '">' + value.content + '</td>';
                    html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#del_order" onclick="del()">刪除</button></td>';
                    html += '</tr>'
                }
            }
            $('#order').html(html);
        },
    })
});
// 刪除訂單
function del() {

    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(6).data('id'));
}


$('#del_order').submit(function (event) {
    event.preventDefault();//不轉頁

    var data = {
        'id': $('#id').val(),
    }
    $.ajax({
        url: base_url + '/shoppingsystem/orderlist/delete', //127.0.0.1:8000/shoppingsystem/product/delete
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                toastr.success("刪除成功");
                setTimeout("window.location.href='index'", 1500);
            }
        },
    });
});