// 下面是顯示所有商品資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/product_show',
        data: { 'product': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (product) {
            let html = '';
            if (typeof (product.retVal) === 'object') {
                for (let i in product.retVal) {
                    let value = product.retVal[i];
                    html += '<tr>';
                    html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                    html += '<td class="text-center">' + value.title + '</td>';
                    html += '<td class="text-center"><img src="' + url + '/' + value.filename + '" width="100" height="100"></td>';
                    html += '<td class="text-center">' + value.price + '</td>';
                    html += '<td class="text-center">' + value.enough + '</td>';
                    // html +=     '<td class="text-center"><select id="enough">';
                    //         // console.log(value.enough);
                    //         // var o = 0;
                    //     for(o=1;o<=value.enough;o++){
                    //         // console.log(o);
                    //         html+= '<option>'+o+'</option>';
                    //         // console.log(o);
                    //     }

                    // html +=     '</select></td>';
                    html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '">' + value.content + '</td>';
                    html += '<td class="text-center"><button class="btn btn-success" type="button" data-toggle="modal" data-target="#product_content" onclick="add()">詳細資訊</button></td>';
                    html += '</tr>'
                }
            }
            $('#product').html(html);
        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});

// 下面是商品詳細資訊
function add() {
    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");
    $('#addtitle').val("");
    $('#addpicture').attr("src", "");
    $('#addimages').val("");
    $('#addprice').val("");
    $('#addenough').val("");
    $('#addcontent').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));
    $('#addtitle').val($(event_td).eq(1).html());
    // $('#addtitle').html($(event_td).eq(1).html());
    $('#addpicture').attr('src', $(event_td).eq(2).find("img").eq(0).attr('src'));
    $('#addimages').val($(event_td).eq(2).data('img'));
    $('#addprice').val($(event_td).eq(3).html());
    $('#addenough').val($(event_td).eq(4).html());
    $('#addcontent').val($(event_td).eq(5).html());
    // CKEDITOR.instances['addcontent'].setData($(event_td).eq(5).data('content'));
}

// 加入購物車 
$('#add_cart').submit(function (event) {
    event.preventDefault();//不轉頁

    var data = {
        'id': $('#id').val(),
    }
    $.ajax({
        url: base_url + '/add_product_cart', //127.0.0.1:8000/add_product_cart
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                // alert("加入成功");
                // window.location.href="product" ;
                toastr.success("加入成功");
                // setTimeout("window.location.href='product'",1500);
            }
            if (result.message === "False") {
                // alert("加入成功");
                // window.location.href="product" ;
                toastr.error("商品已存在");
                // setTimeout("window.location.href='product'",1500);
            }
            // result.value

        },
        error: function (xhr, status, error) {
            console.log(xhr.status.errors);
        }
    });

});