

// 下面是顯示所有商品資料
$(function () {
    //console.log('aaa');
    $.ajax({
        type: 'post',
        url: base_url + '/cart_show',
        data: { 'cart': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (product) {
            console.log(product);
            let html = '';
            if (typeof (product.retVal) === 'object') {
                for (let i in product.retVal) {
                    let value = product.retVal[i];
                    html += '<tr>';
                    html += '<td class="text-center" data-id="' + value._id + '" id="cart_id" name="cart_id">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                    html += '<td class="text-center" id="cart_title" name="cart_title">' + value.title + '</td>';
                    html += '<td class="text-center"><img src="' + url + '/' + value.filename + '" width="100" height="100" id="cart_image" name="cart_image"></td>';
                    html += '<td class="text-center" id="cart_price" name="cart_price">' + value.price + '</td>';
                    html += '<td class="text-center"><select id="cart_enough" name="cart_enough" >';
                    for (o = 1; o <= value.enough; o++) {
                        html += '<option value="' + o + '">' + o + '</option>';
                    }
                    html += '</select></td>';
                    html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '" id="cart_content" name="cart_content">' + value.content + '</td>';
                    html += '<td class="text-center"><button class="btn btn-danger" type="button" data-toggle="modal" data-target="#cart_delete" onclick="del()">刪除</button></td>';
                    html += '</tr>'
                }
            }
            $('#cart').html(html);

        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});
// 下面是結帳 
$('#checkout').submit(function (event) {
    event.preventDefault();//不轉頁

    var optionSelected = $(this).find("option:selected");
    var valueSelected = optionSelected.val();
    // console.log(optionSelected);
    var data = '';
    for (var i = 0; i < optionSelected.length; i++) {
        var car_val = optionSelected[i].value;
        data += car_val
    }
    data += ''
    $.ajax({
        url: base_url + '/cart_checkout', //127.0.0.1:8000/shoppingsystem/product/add
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post', //傳輸方式
        data: {
            'enough': data
        }, //上面data的所有值
        dataType: 'json', //json格式

        success: function (response) {
            console.log(response);
            if (response.message === "Success") {
                toastr.success("購買成功");
                setTimeout("window.location.href='order'", 1500);
            } else {
                toastr.error("購買失敗");
                // setTimeout("window.location.href='order'",1500);
            }
            if (response.message === "Error") {
                toastr.error("請先登入");
                setTimeout("window.location.href='login'", 1500);
            }
            if (response.message === "PLZBuySomething") {
                toastr.error("請先選擇您的商品");
                setTimeout("window.location.href='product'", 1500);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.status.error);
        }
    });
});
// 購物車商品刪除 
function del() {

    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td
    // clear edit diaglog value
    $('#id').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));

}

$('#del').submit(function (event) {
    event.preventDefault();//不轉頁
    var data = {
        'id': $('#id').val(),
    }
    $.ajax({
        url: base_url + '/cart_deletes', //127.0.0.1:8000/deletes
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            if (result.message === "Success") {
                // window.location.href="index" ;
                toastr.success("刪除成功");
                setTimeout("window.location.href='cart'", 1500);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.status.errors);
        }
    });
});
function checkform() {
    if (confirm("確認商品是否正確,若無疑問,請點選確認") == true)
        return true;
    else
        return false;
}

