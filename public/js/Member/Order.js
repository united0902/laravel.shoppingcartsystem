// 下面是顯示訂單資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/order_show',
        data: { 'order': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (order) {
            let html = '';
            if (typeof (order.retVal) === 'object') {
                for (let i in order.retVal) {
                    let value = order.retVal[i];
                    html += '<tr>';
                    html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                    html += '<td class="text-center">' + value.orders_id + '</td>';
                    html += '<td class="text-center">' + value.title + '</td>';
                    html += '<td class="text-center">' + value.price + '</td>';
                    html += '<td class="text-center">' + value.enough + '</td>';
                    html += '<td class="text-center">' + (value.price * value.enough) + '</td>';
                    html += '<td class="text-center">' + value.users + '</td>';
                    html += '<td class="text-center">' + value.created_at + '</td>';
                    html += '</tr>'
                }
            }
            $('#order').html(html);
        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })
});