// 下面是顯示使用者當前會員資料
$(function () {
    $.ajax({
        type: 'get',
        url: base_url + '/users_show',
        dataType: 'json',
        success: function (response) {
            $('#account').val(response.value.account);
            $('#name').val(response.value.name);
            $('#sex').val(response.value.sex);
            $('#email').val(response.value.email);
            $('#phone').val(response.value.phone);
            $('#adds').val(response.value.adds);
        }
    });
});


$('#update').submit(function (event) {
    event.preventDefault();//不轉頁
    // var data = $(this).serialize(); //宣告data為上方createusers所接收的所有欄位值
    var data = {
        'account': $('#account').val(),
        'password': $('#password').val(),
        'password_confirmation': $('#password_confirmation').val(),
        'name': $('#name').val(),
        'sex': $('#sex').val(),
        'adds': $('#adds').val(),
        'email': $('#email').val(),
        'phone': $('#phone').val(),
    }
    $.ajax({
        url: base_url + '/users_update',
        type: 'post', //傳輸方式
        data: data, //上面data的所有值
        dataType: 'json', //json格式
        headers: //避免跨站偽造請求攻擊
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (result) {
            // $('#response').text(result);
            if (result.message === "Success") {

                toastr.success("個人資料修改成功");
                setTimeout("window.location.href='home'", 1500);
            }

        },
        error: function (xhr, status, error) {
            if (xhr.status == 422)//如果他是422的錯誤,就執行以下動作
            {
                var error_msg = ''; //宣告error_msg為空
                $.each(xhr.responseJSON.errors, function (key, value) { //跑迴圈顯示錯誤
                    error_msg += value[0] + "<br>"; //顯示欄位名稱＋錯誤原因並做換行動作
                });
                // alert(error_msg);//彈出視窗跟用戶講哪個欄位錯誤.
                toastr.error(error_msg);
            }
        }

    });
});
