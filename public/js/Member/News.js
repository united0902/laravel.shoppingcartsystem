
CKEDITOR.replace('readcontent', {});

// 下面是顯示所有消息資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/news_show',
        data: { 'news': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (news) {
            let html = '';
            if (typeof (news.retVal) === 'object') {
                for (let i in news.retVal) {
                    let value = news.retVal[i];
                    html += '<tr>';
                    html += '<td class="text-center" data-id="' + value._id + '">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                    html += '<td class="text-center">' + value.title + '</td>';
                    html += '<td class="text-center">' + value.keyword + '</td>';
                    html += '<td class="text-center" style="display:none;" type="hidden" data-content="' + value.content + '">' + value.content + '</td>';
                    html += '<td class="text-center"><button class="btn btn-success" type="button" data-toggle="modal" data-target="#read_news" onclick="read()">詳細內容</button></td>';
                    html += '</tr>'
                }
            }
            $('#news').html(html);

        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});

// 下面是消息詳細資訊
function read() {
    let event_src = this.event.srcElement;         //抓取來源的按鈕
    let event_tr = $(event_src).parent().parent(); //從抓取的按鈕中向上找到了td再來就找到尋找tr
    let event_td = $(event_tr).find('td');         //從已抓取的tr裡面找尋所有的td

    // clear edit diaglog value
    $('#id').val("");
    $('#readtitle').val("");
    $('#readenough').val("");
    $('#readcontent').val("");

    // show value in edit diaglog
    $('#id').val($(event_td).eq(0).data('id'));
    $('#readtitle').val($(event_td).eq(1).html());
    $('#readkeyword').val($(event_td).eq(2).html());
    // $('#readcontent').val($(event_td).eq(3).html());
    CKEDITOR.instances['readcontent'].setData($(event_td).eq(3).data('content'));
}

