// 下面是顯示所有商品資料 
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/shoppingsystem/product',
        data: { 'product': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (product) {
            let html = '';
            for (let i in product) {
                var t = JSON.stringify(product[i]);
                html += '<tr>';
                html += '<td class="text-center">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                html += '<td class="text-center">' + product[i].title + '</td>';
                html += '<td class="text-center"><img src="' + url + '/' + product[i].filename + '" width="100" height="100"></td>';
                html += '<td class="text-center">' + product[i].price + '</td>';
                html += '<td class="text-center">' + product[i].content + '</td>';
                html += '</tr>'
            }
            $('#product').html(html);

        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});

// 下面是顯示所有消息資料 
$(function () {
    //console.log('aaa');
    $.ajax({
        type: 'post',
        url: base_url + '/shoppingsystem/news',
        data: { 'news': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (news) {
            // console.log(news);
            let html = '';
            for (let i in news) {
                // console.log(JSON.stringify(news[i]));
                var t = JSON.stringify(news[i]);
                html += '<tr>';
                html += '<td class="text-center">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                html += '<td class="text-center">' + news[i].title + '</td>';
                html += '<td class="text-center">' + news[i].keyword + '</td>';
                html += '<td class="text-center">' + news[i].content + '</td>';
                html += '</tr>'
            }
            $('#news').html(html);

        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});

// 下面是顯示所有會員資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/shoppingsystem/member',
        data: { 'member': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (member) {
            let html = '';
            for (let i in member) {
                var t = JSON.stringify(member[i]);
                html += '<tr>';
                html += '<td class="text-center">' + ((parseInt(i)) + (parseInt(1))) + '</td>';
                html += '<td class="text-center">' + member[i].account + '</td>';
                html += '<td class="text-center">' + member[i].name + '</td>';
                html += '<td class="text-center">' + member[i].sex + '</td>';
                html += '<td class="text-center">' + member[i].email + '</td>';
                html += '<td class="text-center">' + member[i].adds + '</td>';
                html += '<td class="text-center">' + member[i].phone + '</td>';
                html += '</tr>'
            }
            $('#member').html(html);
        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })

});

// 下面是顯示所有管理員資料
$(function () {
    $.ajax({
        type: 'post',
        url: base_url + '/shoppingsystem/manager',
        data: { 'manager': 'true' },
        dataType: 'json',
        //避免跨站偽造請求攻擊
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (manager) {
            let html = '';
            for (let m in manager) {
                var a = JSON.stringify(manager[m]);
                html += '<tr>';
                html += '<td class="text-center">' + ((parseInt(m)) + (parseInt(1))) + '</td>';
                html += '<td class="text-center">' + manager[m].account + '</td>';
                html += '<td class="text-center">' + manager[m].name + '</td>';
                html += '<td class="text-center">' + manager[m].email + '</td>';
                html += '<td class="text-center">' + manager[m].adds + '</td>';
                html += '<td class="text-center">' + manager[m].phone + '</td>';
                html += '</tr>'
            }
            $('#manager').html(html);
        },
        error: function (jqXHR, status, message) {
            console.log(jqXHR, status, message);
        }
    })
});  
